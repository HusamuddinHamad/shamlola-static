(function (w, $) {

	var Comment = {
		init: function () {
			Comment._prepare();
			Comment._events();
		},

		_prepare: function () {
			Comment.parent 				= $('.article-single');
			Comment.addCommmentButton 	= Comment.parent.find('.add-comment');
		},

		_events: function () {
			Comment.addCommmentButton.click(function (e) {
				e.preventDefault();
				$(this).fadeOut('fast');
				$(this).parents('.else').siblings('.answer-form').fadeIn('fast', function () {
					$(this).find('.avatar')
						.circle();
				});
			});
		}
	}

	$(document).ready(function() {
		$('.article-single .answers .answer .answer-content')
			.expandWithMoreButton(150);

		Comment.init();

		$('.article-single .related-posts')
			.add('.suggested-posts')
			.each(function(index, el) {
				$(el).fooSlide({
					numberOfViews: 			1,
					itemsParentClass: 		'slides',
					nextButtonClass: 		'next-btn',
					previousButtonClass: 	'previous-btn',
					mobile: 				true,
					speed: 					500,		
					paginationContainerClass: 'navigation-item',

					render: function (properties) {
						if ( properties.items.length <= properties.numberOfViews )
							return;

						properties.items.each(function(index, el) {
							var right = (  index-1  ) * $(el).width();
							$(el).css('right', right );
							properties.itemsPositions.push(right);
						});
					},

					action: function (properties, that, index, direction, data, inView, oldData, max, min) {
						if ( properties.items.length <= properties.numberOfViews )
							return;
						
						var inViewOld 	= oldData.indexOf(0);
						var width  		= Math.abs(oldData[that._validateIndex(inViewOld+1)]);

						properties.items.css('display', 'none');
						if ( direction === 1 ) {
							var oldItems = [],
								items 	 = [];

							for(var i=0; i < properties.numberOfViews; i++ ) {
								oldItems.push(
									properties.items[that._validateIndex(inViewOld+i)]
								);

								items.push(
									properties.items[that._validateIndex(inView+i)]
								);
							}

							for( var i=0; i< oldItems.length; i++) {

								$(oldItems[i]).css({
									right: (i*width),
									display: 'block',
								});

								$(oldItems[i]).animate(
									{
										right: ( (i*width)+(width*properties.numberOfViews) ),
									}, 
									properties.speed
								);

								$(items[i]).css({
									right: ( (i*width)-(width*properties.numberOfViews) ),
									display: 'block',
								});
								$(items[i]).animate(
									{
										right: (i*width),
									}, 
									properties.speed
								);
							}
							
						} else {

							var oldItems = [],
								items 	 = [];

							for(var i=0; i< properties.numberOfViews; i++ ) {
								oldItems.push(
									properties.items[that._validateIndex(inViewOld+i)]
								);

								items.push(
									properties.items[that._validateIndex(inView+i)]
								);
							}

							for( var i=0; i< oldItems.length; i++) {

								$(oldItems[i]).css({
									right: (i*width),
									display: 'block',
								});

								$(oldItems[i]).animate(
									{
										right: ( (i*width)-(width*properties.numberOfViews) ),
									}, 
									properties.speed
								);

								$(items[i]).css({
									right: ( (i*width)+(width*properties.numberOfViews) ),
									display: 'block',
								});
								$(items[i]).animate(
									{
										right: ( (i*width) ),
									}, 
									properties.speed
								);
							}

						}
					}
				});
				
			});
	});
})(window, jQuery);