(function ($, w, d) {

	$.fn.customTabs = function () {
		var that = this;
		this.actions = function (selector) {
			$(selector).addClass('active')
				.siblings('.target').removeClass('active');
		}

		$(this).click(function (e) {
			e.preventDefault();
			var target = $(this).data('target');
			var isAlreadyActive = $(this).hasClass('active');
			
			if ( !target || isAlreadyActive )
				return;

			that.actions(target);
			$(this)
				.parents('.tabs')
				.find('[data-target]').parent().removeClass('active');

			$(this).parent().addClass('active');
			$('.block-box-3rd').block('3rd');
		});
	}

	$.fn.expandWithMoreButton = function (length) {
		var that = this;

		if ( typeof(length) !== 'number' )
			return;

		this.each(function(index, el) {
			
			$(el).find('.more').click(function (e) {
				e.preventDefault();
			});

			if ( length < $(el).find('p').text().length ) {
				var originalText = $(el).find('p').text();
				$(el).find('p').text(
					$(el).find('p').text().substr(0, length)+'...'
				);

				$(el).find('.more').click(function (e) {
					e.preventDefault();
					$(el).find('p').text(originalText);
					$(this).css({
						position: 'absolute',
						bottom: 0,
						right:'25px',
						left:'25px',
					}).fadeOut('slow');
				});
			} else {
				$(el).find('.more').addClass('hidden');
			}
		});
	}

	$.fn.circle = function () {
		var that = this;
		function action() {
			$(that).each(function(index, el) {
				var width = $(el).width();
				$(el).css({
					overflow: 'hidden',
					height: width,
					'border-radius': width,
				});
			});
		}

		action();
		$(w).resize(function () {
			action();
		});
	}
	
	$.fn.getAvatar = function () {
		var that = this;
		this.each(function(index, el) {
			if ( $(el).parent().hasClass('shamlola') )
				return;
			
			$.get('//api.randomuser.me', function(data) {
				var image = data.results[0].user.picture;
				$(el).attr('src', image.medium);
			});
		});
	}
	
	$.fn.extend({
		block: function (data) {
			var that = this;
			this._init = function (data) {
				this.prepare(data);
				this.events(data);
			}

			this.prepare = function (data) {
				this.data = data;

				if ( typeof(data) === 'undefined' )
					data = '2nd';

				switch( data ) {
					case '1st':
						this.each(function(index, el) {
							$(el).height( $(el).width() / 1.4 );
						});
						break;
					case '2nd':
						this.each(function(index, el) {
							$(el).height( $(el).width() / 0.7 );
						});
						break;
					case '3rd':
						this.each(function(index, el) {
							$(el).height( $(el).width() );
						});
						break;
					case '4th':
						this.each(function(index, el) {
							$(el).height( $(el).width() / 1.4 );
						});
						break;
					case '5th':
						this.each(function(index, el) {
							$(el).height( $(el).width() / 0.777 );
						});
						break;
				}

			}

			this.events = function (data) {
				$(w).resize(function () {
					that.prepare(data);
				});
			}

			this._init(data);
		}
	});
	
	$.fn.extend({
		menuFit: function (data) {
			var that = this;
			this._init = function (data) {

				if (!this || typeof(data) === 'undefined' ) {
					return null;
				} else {
					this._prepare(data);
					this._action();
					this._events();
				}

			}

			this._events = function () {
				$(w).resize(function(event) {
					that._action();
				});
			}

			this._prepare = function (data) {
				if ( typeof(data) === 'object' ) {
					this._properties = {};

					$.each(data, function(key, value) {
						that._properties[key] = value;
					});
				}

				this.list 			= this.find(this._properties.list);
				this.listItems 		= this.list.children('li');
				this.hiddenList		= this.find(this._properties.hiddenList);
				this.listWidth 		= null;
				this.itemsArray 	= [];
				this.listItems.each(function(index, el) {
					that.itemsArray.push( $(el) );
				});
			}

			this._action = function () {

				this._resetAction();
				if ( $(w).width() < 768 ) {
					this.listItems.last().addClass('hidden');
					return;
				}

				var itemsArray = this.itemsArray.slice(0);
				if ( that.listWidth === null ) {
					that.listWidth = 0;
					
					this.listItems.each(function(index, el) {
						that.listWidth += $(el).outerWidth();
					});
				}

				/*console.log(
					{
						listWidth: this.list.width(),
						width: this.width(),
						hiddenList: this.hiddenList.width(),
					}
				)*/

				if ( this.listWidth + this.hiddenList.width() > this.width() ) {
					var restContainer 	= itemsArray.pop(),
						removedElements = [];

					while( true ) {

						var newWidth = 0;
						removedElements.push(
							itemsArray.pop()
						);

						itemsArray.forEach(function (el, index) {
							newWidth += el.width();
						});

						if ( newWidth + this.hiddenList.width() < that.width() ) {
							this.list.html('');

							itemsArray.reverse().forEach(function (item, i) {
								that.list.append(item);
							});

							restContainer.find('.dropdown-menu').html('');
							removedElements.reverse().forEach(function (item, i) {
								restContainer.find('.dropdown-menu').append(item);
							});

							this.list.append(restContainer);
							restContainer.removeClass('hidden');

							break;
						}
					}

				} else {
					$(this.listItems[this.listItems.length - 1]).addClass('hidden');
				}
			}

			this._resetAction = function () {
				this.list.html('');
				this.itemsArray.forEach(function (el, i) {
					that.list.append( el );
				});
			}

			this._init(data);

		}
	});

	var Header = {
		init: function () {
			Header._prepare();
			Header._events();	
		},

		_prepare: function () {
			Header.element 		= $(document.body).children('header');
			Header.mainNav 		= Header.element.find('nav.main');
			Header.utilities	= Header.element.find('.utilities');
			Header.mainNavLIs 	= Header.mainNav
				.find('>ul>li')
				.add(Header.utilities.find('>li') );

			Header.secondLevel 	= Header.element.children('.second-level');
			Header.collapse 	= Header.secondLevel.find('.collapse-second-level');
			Header.bars 		= Header.secondLevel.find('.sub');

			Header.subLIs 		= Header.bars.find('.list>li');
			
			Header.thirdLevel 	= Header.element.children('.third-level');
			Header.megas 		= Header.thirdLevel.find('.mega');
			Header.subSubLIs	= Header.megas.find('.nav>.sub-sub>li');
		},

		_events: function () {
			var _t, _d = 300;
			Header.mainNavLIs.on(
				'mouseenter',
				function () {
					var bar = $(this).data('header-toggle');

					if ( !bar ) {
						Header.mainNavLIs.removeClass('active');
						Header.secondLevel.finish().slideUp(400);
						return;
					}
					
					if ( /\#/.test(bar) )
						bar = bar.replace('#', '');

					Header.mainNavLIs.not( $(this) ).removeClass('active');
					$(this).addClass('active')

					var matched = Header.bars.filter(function () {
						if ( $(this).attr('id') === bar ) return this;
					});
					
					if ( matched.length < 1 ) {
						Header.secondLevel.finish().slideUp(400);
						return;
					}


					Header.secondLevel.slideDown(400);
					
					Header.bars.finish().css('display', 'none')
						.filter(matched).fadeIn(400);
				}
			);

			Header.subLIs.on(
				'mouseenter',
				function () {

					var megamenu = $(this).data('megamenu');

					if ( !megamenu ) return;
					if ( /\#/.test(megamenu) )
						megamenu = megamenu.replace('#', '');

					$(this).addClass('active')
						.siblings('li')
							.removeClass('active');

					var matched = Header.megas.filter(function () {
						if ( $(this).attr('id') === megamenu ) return this;
					});

					if ( matched.length < 1  ) {
						Header.megas.finish().addClass('hidden').removeClass('active').css('display', 'none');
						Header.thirdLevel.finish().css('display', 'none');
						return;
					}

					if ( matched.hasClass('active') )
						return;

					Header.megas.not(matched).addClass('hidden').removeClass('active').css('display', 'none');
					matched.removeClass('hidden').addClass('active');
					Header.actions.displayData( matched );
					
					matched.css('display', 'block');
					Header.actions.block(
						matched.find('.posts-with-big-thumb article')
					);
					Header.megas.finish().filter(matched).css('display', 'none').fadeIn(450);
				}
			);

			Header.subSubLIs.hover(
				function () {
					$(this).siblings('li')
						.removeClass('active');
					
					$(this).addClass('active');

					var mega = $(this).closest('.mega');
					Header.actions.displayData( mega );
				},
				function () {
					// $(this).removeClass('active');
				}
			)

			Header.collapse.click(function (e) {
				e.preventDefault();
				Header.secondLevel.slideUp(400);
				Header.thirdLevel.css('display', 'none');
				Header.mainNavLIs.removeClass('active');
			});

			$(document).mouseover(function(event) {
				var target = $(event.target);
				var inHeader 		= !!target.closest('header').length;
				var inThirdLevel 	= !!target.closest('.third-level').length;
				var inSecondLevel 	= !!target.closest('.second-level').length;

				if ( !(inThirdLevel || inSecondLevel) ) {
					Header.megas.addClass('hidden').removeClass('active');
					Header.subLIs.removeClass('active');
					Header.thirdLevel.css('display', 'none');
				}
			});

			Header.mainNavLIs
				.filter('.active')
					.trigger('mouseover');
		},

		actions: {
			block: function (el) {
				el.css(
					'height',
					el.width() / 1.4
				);
			},
			displayData: function (el) {

				var dataTabID = el.find('.sub-sub>li.active').data('data');

				if ( !dataTabID ) return; // validator 1
				var dataTabs = el.find('.posts .data');


				var matchedData  = dataTabs.filter(dataTabID);
				if ( matchedData.length < 1 ) return; // validator 2

				Header.thirdLevel.css('display', 'block');
				matchedData.css('display', 'block');
				el.css('display', 'block');
				Header.actions.block( matchedData.find('.posts-with-big-thumb article') );
				dataTabs.css('display', 'none');
				matchedData.fadeIn(200);
			},
		},
	}

	var General = {
		init: function () {
			General._prepare();
			General._events();
		},
		_prepare: function () {
			General.userButton = $('header .user .logged .user-options .user-button');
			General.userOptionsMenu = $('header .user .logged .user-options');
			General.askShamlola = $('.ask-shamlola .ask-form');

			General.notificationsButton = $('header .user .logged .notifications');
		},
		_events: function () {
			
			General.userButton.on('click', function (e) {
				e.preventDefault();
				General.userOptionsMenu.toggleClass('active');
			});
			
			General.notificationsButton.on('click', function (e) {
				e.preventDefault();
				$(this).toggleClass('active');
			});
			
			$(d).click(function(e) {
				var clickover = $(e.target);
				var isActive = General.userOptionsMenu.hasClass('active');
				var isActive2 = General.notificationsButton.hasClass('active');

				if (  isActive && clickover.parents('.user-options').length === 0 ) {
					General.userButton.click();
				}

				if (  isActive2 && clickover.parents('.notifications').length === 0 ) {
					General.notificationsButton.click();
				}
			});

			General.askShamlola.find('.ask-button').click(function (e) {
				e.preventDefault();

				$(this).fadeOut('slow');
				General.askShamlola.find('form')
					.css('display', 'none')
					.removeClass('hidden')
					.slideDown('slow')
					.find('input, textarea').on('keyup', function(event) {
						var title 		= General.askShamlola.find('[name=title]');
						var description = General.askShamlola.find('[name=description]');

						if ( title.val().length > 0 && description.val().length > 0 ) {
							General.askShamlola.find('.submit').removeAttr('disabled');
						} else {
							General.askShamlola.find('.submit').attr('disabled', 'disabled');
						}
					});
			});
		}
	}

	$.fn.youtubePlayer = function () {
		
		if ( !this.length )
			return;
		
		var that = this;
		this.playButton 		= this.find('.play-button');
		this.video 				= this.data('youtube');
		this.youtubePlayer 		= null;
		this.playerDomContainer = null;

		this._prepare = function () {

			this.playerDomContainer 	= $('<div class="ytplayer-container"></div>');
			var playerDom 				= $('<div id="ytplayer"></div>');
			this.playerDomContainer.css({
				position: 'absolute',
				top:0,
				left:0,
				right:0,
				bottom:'58px',
			}).addClass('hidden');

			playerDom.css({
				width: '100%',
				height: '100%',
			});

			this.playerDomContainer.append(playerDom)
			this.prepend(
				this.playerDomContainer
			);

			this._getYoutubeAPIReady();
		}

		this._events = function () {
			that.playButton.click(function(event) {
				event.preventDefault();
				$(this).addClass('hidden');
				that.playerDomContainer.removeClass('hidden');
				that.youtubePlayer.playVideo();
			});
		}

		this._getYoutubeAPIReady = function () {

			$.getScript(
				'//www.youtube.com/iframe_api',
				function () {
					w.onYouTubeIframeAPIReady = function () {
						that.youtubePlayer = new YT.Player('ytplayer', {
							videoId: that.video,
							events: {
								onReady: that._events
							}
						});
					}
				}
			);

		}

		// triggers
		this._prepare();
	}

	$.fn.imagizer = function () {
		this.each(function(index, el) {
			if ( ! $(el).data('image') )
				return;

			$(el).css({
				display: 'block',
				'background-image': 'url(' + $(el).data('image') + ')',
				'background-size': 'cover',
				'background-repeat': 'no-repeat',
				'background-position': 'center center',
			});
		});
	}

/*
	$('.recipes article').block('5th');
	$('[data-image]').imagizer();
*/


	$(document).ready(function () {
		$('.block-box-1st').block('1st');
		$('.block-box-3rd, .grid article').block('3rd');
		$('.block-box-4th').block('4th');
		$('.block-box-5rd').block('5th');
		$('[data-target]').customTabs();
		$('[data-youtube]').youtubePlayer();
		$('[data-image]').imagizer();

		/*$('.third-menu').menuFit({
			list: '.third-menu-list',
			hiddenList: '.more-list'
		});*/

		Header.init();
		General.init();

	});

})(jQuery, window, document);