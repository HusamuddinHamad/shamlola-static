(function (w, $) {
	var suggestedSectionActions = function () {
		$('.suggested .more').on('click', function (e) {
			e.preventDefault();
			$(this).find('.glyphicon').toggleClass('glyphicon-chevron-left');
			$(this).find('.glyphicon').toggleClass('glyphicon-chevron-right');
			$('.suggested .items').toggleClass('active');

			$('.suggested .items .items-container').toggleClass('relative');
			
			$('.suggested .items .item').last().toggleClass('hidden-md');
			$('.suggested .items .item').last().toggleClass('hidden-lg');

			$('.suggested .items .item').each(function(index, el) {
				$(el).height( $(el).width() );
			});
		});
	}

	var recipeSlider = function () {
		var theSlider = $('.recipe-single .single-slider');
		var thumbnailsHolder = theSlider.find('.thumbnails-holder');
		var items = theSlider.find('.thumbnails .thumbnail');

		thumbnailsHolder.width(
			(items.length * 60) + items.length
		);

		function maintainRatio () {
			theSlider.height(
				theSlider.width() / 1.6
			);
		}
		
		maintainRatio();
		$(w).resize( maintainRatio );

		theSlider.fooSlide({
			numberOfViews: 			1,
			itemsParentClass: 		'sliders',
			nextButtonClass: 		'next',
			previousButtonClass: 	'previous',
			mobile: 				true,
			speed: 					500,	
			pagination: 			true,

			render: function (properties, that) {

				if ( properties.items.length <= properties.numberOfViews )
					return;


				properties.items.each(function(index, el) {
					var right = (  index-1  ) * $(el).width();
					properties.itemsPositions.push(right);


					$(properties.items[ that._validateIndex(index) ]).css('right', right );
				});

				var thumbs = that.find('.thumbnails .thumbnails-holder .thumbnail');
				thumbs.each(function (index, el) {
					$(el).attr( 'data-paged', that._validateIndex(index) );
				});
			},

			action: function (properties, that, index, direction, data, inView, oldData, max, min) {
				if ( properties.items.length <= properties.numberOfViews )
					return;

				var inViewOld 	= oldData.indexOf(0);
				var width  		= Math.abs(oldData[that._validateIndex(inViewOld+1)]);
				var thumbs 		= that.find('.thumbnails .thumbnails-holder .thumbnail');

				thumbs.removeClass('active');
				$(thumbs[that._validateIndex(inView-1)]).addClass('active');

				properties.items.css('display', 'none');
				if ( direction > 0 ) {
					$(properties.items[ that._validateIndex(inViewOld) ]).css({
						display:'block',
						right:0,
					});
					$(properties.items[ that._validateIndex(inViewOld) ]).animate({
						right: width
					}, properties.speed);

					$(properties.items[ that._validateIndex(inView) ]).css({
						right: -width,
						display:'block',
					});
					$(properties.items[ that._validateIndex(inView) ]).animate({
						right: 0
					}, properties.speed);

				} else {
					$(properties.items[ that._validateIndex(inViewOld) ]).css({
						display: 'block',
						right:0,
					});
					$(properties.items[ that._validateIndex(inViewOld) ]).animate({
						right: -width
					}, properties.speed);

					$(properties.items[ that._validateIndex(inView) ]).css({
						display:'block',
						right:width,
					});
					$(properties.items[ that._validateIndex(inView) ]).animate({
						right: 0
					}, properties.speed);
				}
				
			}
		});
	}


	var Comment = {
		init: function () {
			Comment._prepare();
			Comment._events();
		},

		_prepare: function () {
			Comment.parent 				= $('.recipe-single');
			Comment.addCommmentButton 	= Comment.parent.find('.add-comment');
		},

		_events: function () {
			Comment.addCommmentButton.click(function (e) {
				e.preventDefault();
				$(this).fadeOut('fast');
				$(this).parents('.else').siblings('.answer-form').fadeIn('fast', function () {
					$(this).find('.avatar')
						.circle();
				});
			});
		}
	}

	var adjustMiniSlider = function () {
		$('.video-section-slider').height(
			$('.video-section-slider').height(
				$('.video-section-slider .item').height() + 50
			)
		);
	} 

	$(document).ready(function() {
		suggestedSectionActions();
		recipeSlider();


		adjustMiniSlider();
		$(w).resize(function () {
			adjustMiniSlider();
		});
		
		$('.video-section-slider').fooSlide({
			itemsParentClass: 'slides',
			nextButtonClass: 'next',
			previousButtonClass: 'previous',
			paginationContainerClass: 'nav-pagination',
			speed: 600,
			numberOfViews: {
				xs: 1,
				sm: 2,
				md: 3,
				lg: 4,
			},
			mobile: true,
			render: function (properties) {
				if ( properties.items.length > properties.numberOfViews ) {
					properties.items.each(function(index, el) {
						var right = (index-properties.numberOfViews) * $(el).outerWidth();
						properties.itemsPositions.push(right);
						$(el).css('right', right );
					});
				} else {
					properties.items.each(function(index, el) {
						var right = (index) * $(el).outerWidth();
						$(el).css('right', right );
					});
				}
			},
			action: function (properties, that, index, direction, data, inView, oldData, max, min) {
				
				if ( properties.items.length <= properties.numberOfViews )
					return;

				var width 		= data[inView+1],
					inViewOld 	= oldData.indexOf(0);

				properties.items.css('display', 'none');
				if ( direction === 1 ) {
					var oldItems = [],
						items 	 = [];

					for(var i=0; i < properties.numberOfViews; i++ ) {
						oldItems.push(
							properties.items[that._validateIndex(inViewOld+i)]
						);

						items.push(
							properties.items[that._validateIndex(inView+i)]
						);
					}

					for( var i=0; i< oldItems.length; i++) {

						$(oldItems[i]).css({
							right: (i*width),
							display: 'block',
						});

						$(oldItems[i]).animate(
							{
								right: ( (i*width)+(width*properties.numberOfViews) ),
							}, 
							properties.speed
						);

						$(items[i]).css({
							right: ( (i*width)-(width*properties.numberOfViews) ),
							display: 'block',
						});
						$(items[i]).animate(
							{
								right: (i*width),
							}, 
							properties.speed
						);
					}
					
				} else {

					var oldItems = [],
						items 	 = [];

					for(var i=0; i< properties.numberOfViews; i++ ) {
						oldItems.push(
							properties.items[that._validateIndex(inViewOld+i)]
						);

						items.push(
							properties.items[that._validateIndex(inView+i)]
						);
					}

					for( var i=0; i< oldItems.length; i++) {

						$(oldItems[i]).css({
							right: (i*width),
							display: 'block',
						});

						$(oldItems[i]).animate(
							{
								right: ( (i*width)-(width*properties.numberOfViews) ),
							}, 
							properties.speed
						);

						$(items[i]).css({
							right: ( (i*width)+(width*properties.numberOfViews) ),
							display: 'block',
						});
						$(items[i]).animate(
							{
								right: ( (i*width) ),
							}, 
							properties.speed
						);
					}

				}

			},
			// autoplay: 2000,
		});


		$('.recipe-single .answers .answer .answer-content')
			.expandWithMoreButton(150);

		Comment.init();
		$('.recipe-single .related-posts')
			.add('.suggested-posts')
			.each(function(index, el) {
				$(el).fooSlide({
					numberOfViews: 			1,
					itemsParentClass: 		'slides',
					nextButtonClass: 		'next-btn',
					previousButtonClass: 	'previous-btn',
					mobile: 				true,
					speed: 					500,		
					paginationContainerClass: 'navigation-item',

					render: function (properties) {
						if ( properties.items.length <= properties.numberOfViews )
							return;

						properties.items.each(function(index, el) {
							var right = (  index-1  ) * $(el).width();
							$(el).css('right', right );
							properties.itemsPositions.push(right);
						});
					},

					action: function (properties, that, index, direction, data, inView, oldData, max, min) {
						if ( properties.items.length <= properties.numberOfViews )
							return;
						
						var inViewOld 	= oldData.indexOf(0);
						var width  		= Math.abs(oldData[that._validateIndex(inViewOld+1)]);

						properties.items.css('display', 'none');
						if ( direction === 1 ) {
							var oldItems = [],
								items 	 = [];

							for(var i=0; i < properties.numberOfViews; i++ ) {
								oldItems.push(
									properties.items[that._validateIndex(inViewOld+i)]
								);

								items.push(
									properties.items[that._validateIndex(inView+i)]
								);
							}

							for( var i=0; i< oldItems.length; i++) {

								$(oldItems[i]).css({
									right: (i*width),
									display: 'block',
								});

								$(oldItems[i]).animate(
									{
										right: ( (i*width)+(width*properties.numberOfViews) ),
									}, 
									properties.speed
								);

								$(items[i]).css({
									right: ( (i*width)-(width*properties.numberOfViews) ),
									display: 'block',
								});
								$(items[i]).animate(
									{
										right: (i*width),
									}, 
									properties.speed
								);
							}
							
						} else {

							var oldItems = [],
								items 	 = [];

							for(var i=0; i< properties.numberOfViews; i++ ) {
								oldItems.push(
									properties.items[that._validateIndex(inViewOld+i)]
								);

								items.push(
									properties.items[that._validateIndex(inView+i)]
								);
							}

							for( var i=0; i< oldItems.length; i++) {

								$(oldItems[i]).css({
									right: (i*width),
									display: 'block',
								});

								$(oldItems[i]).animate(
									{
										right: ( (i*width)-(width*properties.numberOfViews) ),
									}, 
									properties.speed
								);

								$(items[i]).css({
									right: ( (i*width)+(width*properties.numberOfViews) ),
									display: 'block',
								});
								$(items[i]).animate(
									{
										right: ( (i*width) ),
									}, 
									properties.speed
								);
							}

						}
					}
				});
				
			});
	});
})(window, jQuery);