(function (w, d, $) {

	var Recipes = {
		init: function () {
			Recipes._prepare();
			Recipes._events();
		},
		_prepare: function () {
			Recipes.container = $('.recipes-template')
		}, 
		_events: function () {
			Recipes.container.find('.articles article').block('5th');
		}
	}

	$(document).ready(function (e) {
		Recipes.init();
	});

})(window, document, jQuery);