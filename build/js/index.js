(function (d, w, $) {

	var Videos = {
		init: function () {
			Videos._prepare();
			Videos._events();
			Videos._getYoutubeAPIReady();
		},

		_prepare: function () {
			Videos.parent 		= $('.index');
			Videos.videos 		= Videos.parent.find('.videos');
			Videos.articles 	= Videos.videos.find('article');
			Videos.player 		= Videos.videos.find('.player');
			Videos.playButton 	= Videos.player.find('.play-button');
			Videos.youtubePlayerDOM = Videos.player.find('#ytplayer');
			Videos.youtubePlayer;

			var data = Videos.articles.filter('.active').data('data');
			data = eval( data );

			if ( typeof(data) !== 'object' )
					return;

			Videos._getVideoReady(
				data
			);		
		},

		_events: function () {

			Videos.playButton.click(function (e) {
				e.preventDefault();
			});

			Videos.articles.find('a').click(function (e) {
				e.preventDefault();
				var parentArticle = $(this).parents('article');

				if ( parentArticle.hasClass('active') )
					return;

				Videos.articles.removeClass('active');
				parentArticle.addClass('active');


				var data = parentArticle.data('data');
				data = eval( data );

				if ( typeof(data) !== 'object' )
					return;

				$('#ytplayer').addClass('hidden');
				Videos.playButton.removeClass('hidden');
				Videos._getVideoReady(data);
			});
		},

		_getVideoReady: function (data) {

			// background changing
			Videos.player.find('.screen .img').css(
				{
					'background-image': 'url('+ data.image +')',
				}
			);

			// title change
			Videos.player.find('.text h3').text( data.title );
			Videos.player.find('.text h3').parent('a').attr('href', data.permalink);
			
			// date
			moment.locale('ar-EG');
			Videos.player.find('.text .details .date').text(
				moment(data.date).fromNow()
			);
			
			// rate
			var startsTemplate = _.template( $('#stars-template').html() );


			Videos.player.find('.text .details .rate').html(
				startsTemplate({
					stars: data.rate
				})
			);

			if ( Videos.youtubeAPIReady )
				Videos.youtubePlayer.loadVideoById(data.video).stopVideo();
			
		},

		_getYoutubeAPIReady: function () {

			function _events() {
				Videos.youtubeAPIReady = true;
				Videos.playButton.click(function() {
					$(this).addClass('hidden');
					$('#ytplayer').removeClass('hidden');
					Videos.youtubePlayer.playVideo();
				});

				var data = Videos.articles.filter('.active').data('data');
				data = eval( data );

				if ( typeof(data) !== 'object' )
						return;

				Videos._getVideoReady(
					data
				);	
			}

			$.getScript(
				'//www.youtube.com/iframe_api',
				function () {
					w.onYouTubeIframeAPIReady = function () {
						Videos.youtubePlayer = new YT.Player('ytplayer', {
							events: {
								onReady: _events
							}
						});
					}
				}
			);
		}
	}

	$(document).ready(function () {
		$('.avatar').circle();
		$('.avatar img').getAvatar();
		$('.index .recipes article').block('5th');
		Videos.init();
	});

})(document, window, jQuery);