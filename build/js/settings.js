(function ($, w, d) {

	Utility = {
		tabs: function (e) {
			e.preventDefault();
			var tab 	= $(this).data('tab');
			var page = e.data.page;

			// validate
			if ( !tab ) return;
			if ( $(tab).length < 1 ) return;
			if ( $(this).parent().hasClass('active') ) return;

			// toggle active class
			$(this).parent().addClass('active').siblings().removeClass('active');
		

			page.find('.tab.active')
				.slideUp('slow', function () {
						page.find('.tab.hidden')
							.css('display', 'none')
							.removeClass('hidden')
							.addClass('active')
							.slideDown('slow');

						$(this)
							.removeClass('active')
							.addClass('hidden');
				});

		}
	}

	SettingsTemplate = {
		init: function () {
			SettingsTemplate._prepare();
			SettingsTemplate._events();
		},
		_prepare: function () {
			SettingsTemplate.page = $('.settings-template');
			SettingsTemplate.navButtons = SettingsTemplate.page.find('nav ul li a');
		},
		_events: function () {
			SettingsTemplate.navButtons.click( {page: SettingsTemplate.page}, Utility.tabs );
		}
	}

	$(d).ready(function () {
		SettingsTemplate.init();
	});

})(jQuery, window, document);