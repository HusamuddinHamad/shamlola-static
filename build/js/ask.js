(function ($, w, d) {

	var Single = {
		init: function () {
			Single._prepare();
			Single._events();
		},

		_prepare: function () {
			Single.parent 				= $('.single-ask');
			Single.addCommmentButton 	= Single.parent.find('.add-comment');
		},

		_events: function () {
			Single.addCommmentButton.click(function (e) {
				e.preventDefault();
				$(this).fadeOut('fast');
				$(this).parents('.else').siblings('.answer-form').fadeIn('fast', function () {
					$(this).find('.avatar')
						.circle();
				});
			});
		}
	}

	$(d).ready(function () {
		$('.single-ask .avatar').circle();
		$('.single-ask .answers:not(.shamlola-answer)')
			.find('.avatar img')
			.add('.answer-form .avatar img')
			.add('.main .header .avatar img')
			.getAvatar();

		$('.single-ask .answers:not(.shamlola-answer)')
			.find('.answer .answer-content')
			.expandWithMoreButton(150);

		Single.init();
	});

})(jQuery, window, document);