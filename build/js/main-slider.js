(function (w, $) {

	w.Components = {};
	w.Components.MainSlider = {
		init: function () {
			w.Components.MainSlider.prepare();
			w.Components.MainSlider.events();
		},

		prepare: function () {
			w.Components.MainSlider.slider = $('div.main-slider');
			w.Components.MainSlider.resizing();
		},

		events: function () {
			$(w).on('resize', function () {
				w.Components.MainSlider.resizing();
			});
		},

		resizing: function () {
			w.Components.MainSlider.slider.height(
				w.Components.MainSlider.slider.children('.container').first().width() / 2.6
			);
		}

	}


	$(document).ready(function () {
		
		$('.main-slider').fooSlide({
			itemsParentClass: 'slides',
			nextButtonClass: 'next',
			previousButtonClass: 'previous',
			mobile: true,
			speed: 700,
			autoplay: true,
			render: function (properties) {
				properties.items.each(function(index, el) {
					var right = (  index-1 ) * $(el).width();
					properties.itemsPositions.push(right);
					$(el).css('right', right );
				});

				properties.items.find('.slider-content .play-button').css('display', 'none');
				properties.items.find('.slider-content > div').css('display', 'none');
				$( properties.items[1] ).find('.slider-content > div').fadeIn(700);
				$( properties.items[1] ).find('.slider-content .play-button').fadeIn(700);

			},
			action: function (properties, that, index, direction, data, inView, oldData, max, min) {

				var inViewOld 	= oldData.indexOf(0);
				var width  		= Math.abs(oldData[that._validateIndex(inViewOld+1)]);
				var maxOld 		= oldData.indexOf( Math.max.apply(null, oldData) );
				var minOld 		= oldData.indexOf( Math.min.apply(null, oldData) );

				properties.items.css('display', 'none');
				if ( direction > 0 ) {
					// right
					$(properties.items[that._validateIndex(inViewOld-1)]).css({
						right: -1*width,
						display:'block',
					}).animate({
						right: 0,
					}, properties.speed);

					// center
					$(properties.items[that._validateIndex(inViewOld)]).css({
						right:0,
						display:'block',
					}).animate({
						right:width,
					}, properties.speed);

					// left
					$(properties.items[that._validateIndex(maxOld)]).css({
						right:-2*width,
						display:'block',
					}).animate({
						right:-1*width,
					}, properties.speed);

					var clone = $(properties.items[that._validateIndex(inViewOld+1)]).clone();
					clone.appendTo(properties.itemsContainer);

					clone.css({
						right:width,
						display:'block',
					}).animate({
						right:2*width,
					}, properties.speed, 'swing', function () {
						$(this).remove();
					});

					properties.items.find('.data').css('display', 'none');
					properties.items.find('.play-button').css('display', 'none');
					$(properties.items[that._validateIndex(inViewOld-1)]).find('.data').fadeIn(600);
					$(properties.items[that._validateIndex(inViewOld-1)]).find('.play-button').fadeIn(600);

				} else {
					// right
					$(properties.items[that._validateIndex(minOld)]).css({
						right: 2*width,
						display:'block',
					}).animate({
						right: width,
					}, properties.speed);
					
					var clone = $(properties.items[that._validateIndex(inViewOld-1)]).clone();
					clone.appendTo(properties.itemsContainer);
					clone.css({
						right:-1*width,
						display:'block',
					}).animate({
						right:-2*width,
					}, properties.speed, 'swing', function () {
						$(this).remove();
					});

					// center
					$(properties.items[that._validateIndex(inViewOld)]).css({
						right:0,
						display:'block',
					}).animate({
						right:-1*width,
					}, properties.speed);

					// left
					$(properties.items[that._validateIndex(inViewOld+1)]).css({
						right:1*width,
						display:'block',
					}).animate({
						right:0,
					}, properties.speed);

					properties.items.find('.data').css('display', 'none');
					properties.items.find('.play-button').css('display', 'none');
					$(properties.items[that._validateIndex(inViewOld+1)]).find('.data').fadeIn(600);
					$(properties.items[that._validateIndex(inViewOld+1)]).find('.play-button').fadeIn(600);
				}

				/*if ( direction === 1 )
					$(properties.items[ data.indexOf(min) ]).css('display', 'none');
				else
					$(properties.items[ data.indexOf(max) ]).css('display', 'none');

				var inView = data.indexOf(0);
				properties.items.each(function(index, el) {

					if ( index !== inView )
						$(el).find('.slider-content > div').fadeOut(that._properties.speed);
					else
 						$(el).find('.slider-content > div').fadeIn(that._properties.speed);


					$(el).stop(false, true);
					$(el).animate({
						right: data[index],
 					}, properties.speed, function () {
 						$(el).css('display', 'block');
 					});

				});*/
				
			}
		});

		Components.MainSlider.init();
	});

})(window, jQuery);