(function ($, d, w) {

	var Single = {
		init: function () {
			Single._prepare();
			Single._events();
		},

		_prepare: function () {
			Single.parent 				= $('.community-single');
			Single.addCommmentButton 	= Single.parent.find('.add-comment');
		},

		_events: function () {
			Single.addCommmentButton.click(function (e) {
				e.preventDefault();
				$(this).fadeOut('fast');
				$(this).parents('.else').siblings('.answer-form').fadeIn('fast', function () {
					$(this).find('.avatar')
						.circle();
				});
			});
		}
	}

	$(d).ready(function () {
		Single.init();
		$('.community-single .answers article .answer-content')
			.expandWithMoreButton(150);
	});

})(jQuery, document, window);