(function ($, w, d) {

	$.fn.bookSlider = function (args) {
		var that = this;
		var _properties = {
			sliderContainer: 	'.slides',
			mask: 				'.mask',
			nextButton:			'.next',
			prevButton: 		'.prev',
			item: 				'.item',
			itemsPerView: 		3,
			index: 				0,
			items: 				null,
			count: 				null,
			stepWidth: 			null,
		};

		this.init = function (args) {
			var matchAslide = this._prepare(args);
			if ( !matchAslide ) {
				$(this).css({
					'padding-right': 0,
					'padding-left': 0,
				});
				// $(_properties.nextButton).add( $(_properties.prevButton) ).css('display', 'none');
				return;
			}

			this._events();
		}

		this._prepare = function (args) {
			
			// replace the defaults
			if ( typeof(args) === 'object' ) {

				for( var property in args ) {
					if ( !args[property] )
						continue;

					switch( property ) {
						case 'sliderContainer':
						case 'item':
						case 'prevButton':
						case 'mask':
						case 'nextButton':
							_properties[property] = args[property];
							break;
					}
				}

			}

			// sliderContainer
			_properties.sliderContainer = this.find(_properties.sliderContainer);
			// items
			var _items = _properties.sliderContainer.find(_properties.item);
			_properties.count = _items.length;

			// validate the slider
			if ( _properties.itemsPerView >= _properties.count )
				return false;

			// set _properties
			for( var property in _properties ) {
				switch( property ) {
					case 'stepWidth':
						_properties[property] = _properties.mask.width() / 3;
						break;
					case 'items':
						_properties.items = _items.add(
							_items.clone().addClass('clone')
						);
						_properties.sliderContainer.prepend( _properties.items );
						break;
					case 'prevButton':
					case 'nextButton':
					case 'mask':
						if ( !_properties[property] )
							continue;

						_properties[property] = this.find(args[property]);
						break;
				}
			}

			_properties.items.css('width', _properties.stepWidth);

			_properties.sliderContainer.css({
				width: _properties.stepWidth * _properties.items.length,
				transition: '0.4s',
				'-webkit-transition': '0.4s',
				'-moz-transition': '0.4s',
				'-o-transition': '0.4s',
				transform: 'translate3d('+
					parseFloat(
						// _properties.items.length*_properties.stepWidth/2
						0
					)
				+'px,0,0)',
			});
			
			return true;
		}

		this._events = function () {
			_properties.nextButton.click(function (e) {
				e.preventDefault();
				that._validateIndex(++_properties.index);
				that._action();
			});

			_properties.prevButton.click(function (e) {
				e.preventDefault();
				that._validateIndex(--_properties.index);
				that._action();
			});

			this.on('swipeleft', function () {
				_properties.prevButton.trigger('click');
			});

			this.on('swiperight', function () {
				_properties.nextButton.trigger('click');
			});

			$(w).resize(function () {
				_properties.stepWidth = _properties.mask.width() / 3;
				_properties.items.css('width',  _properties.stepWidth );
				_properties.sliderContainer.css(
					'width',
					_properties.items.length*_properties.stepWidth
				);

				_properties.sliderContainer.css({
					transition: 'initial',
					'-webkit-transition': 'initial',
					'-moz-transition': 'initial',
					'-o-transition': 'initial',
					transform: 'translate3d(0,0,0)',
				});
			});
		}

		this._action = function () {

			_properties.sliderContainer.css({
				transition: '0.4s',
				'-webkit-transition': '0.4s',
				'-moz-transition': '0.4s',
				'-o-transition': '0.4s',
				transform: 'translate3d(' + 
					
					parseFloat(
						_properties.stepWidth * _properties.index
					)

					+ 'px'
					+', 0,0)',
			});
		}

		this._validateIndex = function (index) {

			if ( index === undefined )
				index = _properties.index;

			var oldIndex = index;

			if ( index <= -1 ) {
				index = _properties.count - ( Math.abs(index) % _properties.count );
			}

			return _properties.index = index %= _properties.count;

		}

		this.init(args);
	}
	
	var adjustMiniSlider = function () {
		$('.others-section-slider').height(
			$('.others-section-slider').height(
				$('.others-section-slider .item').height() + 50
			)
		);
	}


	var ShowThumbnailBigger = {
		init: function () {
			ShowThumbnailBigger._prepare();
			ShowThumbnailBigger._events();
		},

		_prepare: function () {
			ShowThumbnailBigger.element 			= $('.single-book .article-section .main .images');
			ShowThumbnailBigger.smallThumbails 	= ShowThumbnailBigger.element.find('.bordered');
			ShowThumbnailBigger.bigThumnail 		= {
				img: 		ShowThumbnailBigger.element.find('.cover-in-view img'),
				anchor: 	ShowThumbnailBigger.element.find('.cover-in-view .expand-button'),
			};
		},

		_events: function () {
			ShowThumbnailBigger.smallThumbails.click(function(event) {
				event.preventDefault();
				ShowThumbnailBigger.bigThumnail.img.attr(
					'src', 
					$(this).attr('href')
				);
				ShowThumbnailBigger.bigThumnail.anchor.attr(
					'href', 
					$(this).attr('href')
				);
			});
		}

	}

	$(d).ready(function () {

		ShowThumbnailBigger.init();

		adjustMiniSlider();
		$(w).resize(function () {
			adjustMiniSlider();
		});
		
		$('.others-section-slider').fooSlide({
			itemsParentClass: 'slides',
			nextButtonClass: 'next',
			previousButtonClass: 'previous',
			paginationContainerClass: 'nav-pagination',
			speed: 600,
			numberOfViews: {
				xs: 1,
				sm: 2,
				md: 3,
				lg: 4,
			},
			mobile: true,
			render: function (properties) {
				if ( properties.items.length > properties.numberOfViews ) {
					properties.items.each(function(index, el) {
						var right = (index-properties.numberOfViews) * $(el).outerWidth();
						properties.itemsPositions.push(right);
						$(el).css('right', right );
					});
				} else {
					properties.items.each(function(index, el) {
						var right = (index) * $(el).outerWidth();
						$(el).css('right', right );
					});
				}
			},
			action: function (properties, that, index, direction, data, inView, oldData, max, min) {
				
				if ( properties.items.length <= properties.numberOfViews )
					return;

				var width 		= data[inView+1],
					inViewOld 	= oldData.indexOf(0);

				properties.items.css('display', 'none');
				if ( direction === 1 ) {
					var oldItems = [],
						items 	 = [];

					for(var i=0; i < properties.numberOfViews; i++ ) {
						oldItems.push(
							properties.items[that._validateIndex(inViewOld+i)]
						);

						items.push(
							properties.items[that._validateIndex(inView+i)]
						);
					}

					for( var i=0; i< oldItems.length; i++) {

						$(oldItems[i]).css({
							right: (i*width),
							display: 'block',
						});

						$(oldItems[i]).animate(
							{
								right: ( (i*width)+(width*properties.numberOfViews) ),
							}, 
							properties.speed
						);

						$(items[i]).css({
							right: ( (i*width)-(width*properties.numberOfViews) ),
							display: 'block',
						});
						$(items[i]).animate(
							{
								right: (i*width),
							}, 
							properties.speed
						);
					}
					
				} else {

					var oldItems = [],
						items 	 = [];

					for(var i=0; i< properties.numberOfViews; i++ ) {
						oldItems.push(
							properties.items[that._validateIndex(inViewOld+i)]
						);

						items.push(
							properties.items[that._validateIndex(inView+i)]
						);
					}

					for( var i=0; i< oldItems.length; i++) {

						$(oldItems[i]).css({
							right: (i*width),
							display: 'block',
						});

						$(oldItems[i]).animate(
							{
								right: ( (i*width)-(width*properties.numberOfViews) ),
							}, 
							properties.speed
						);

						$(items[i]).css({
							right: ( (i*width)+(width*properties.numberOfViews) ),
							display: 'block',
						});
						$(items[i]).animate(
							{
								right: ( (i*width) ),
							}, 
							properties.speed
						);
					}

				}

			},
			// autoplay: 2000,
		});

		$('.article-section article .covers-slider').bookSlider({
			sliderContainer: 	'.slides',
			item: 				'.image-holder',
			nextButton: 		'.next',
			prevButton: 		'.prev',
			mask: 				'.mask',
		});

		$('.cover-in-view a').magnificPopup({
			type: 'image',
		});

		

		$('.suggested-books').fooSlide({
			numberOfViews: 			1,
			itemsParentClass: 		'slides',
			nextButtonClass: 		'next-btn',
			previousButtonClass: 	'previous-btn',
			mobile: 				true,
			speed: 					500,		
			paginationContainerClass: 'navigation-item',

			render: function (properties) {
				if ( properties.items.length <= properties.numberOfViews )
					return;

				properties.items.each(function(index, el) {
					var right = (  index-1  ) * $(el).width();
					$(el).css('right', right );
					properties.itemsPositions.push(right);
				});
			},

			action: function (properties, that, index, direction, data, inView, oldData, max, min) {
				if ( properties.items.length <= properties.numberOfViews )
					return;
				
				var inViewOld 	= oldData.indexOf(0);
				var width  		= Math.abs(oldData[that._validateIndex(inViewOld+1)]);

				properties.items.css('display', 'none');
				if ( direction === 1 ) {
					var oldItems = [],
						items 	 = [];

					for(var i=0; i < properties.numberOfViews; i++ ) {
						oldItems.push(
							properties.items[that._validateIndex(inViewOld+i)]
						);

						items.push(
							properties.items[that._validateIndex(inView+i)]
						);
					}

					for( var i=0; i< oldItems.length; i++) {

						$(oldItems[i]).css({
							right: (i*width),
							display: 'block',
						});

						$(oldItems[i]).animate(
							{
								right: ( (i*width)+(width*properties.numberOfViews) ),
							}, 
							properties.speed
						);

						$(items[i]).css({
							right: ( (i*width)-(width*properties.numberOfViews) ),
							display: 'block',
						});
						$(items[i]).animate(
							{
								right: (i*width),
							}, 
							properties.speed
						);
					}
					
				} else {

					var oldItems = [],
						items 	 = [];

					for(var i=0; i< properties.numberOfViews; i++ ) {
						oldItems.push(
							properties.items[that._validateIndex(inViewOld+i)]
						);

						items.push(
							properties.items[that._validateIndex(inView+i)]
						);
					}

					for( var i=0; i< oldItems.length; i++) {

						$(oldItems[i]).css({
							right: (i*width),
							display: 'block',
						});

						$(oldItems[i]).animate(
							{
								right: ( (i*width)-(width*properties.numberOfViews) ),
							}, 
							properties.speed
						);

						$(items[i]).css({
							right: ( (i*width)+(width*properties.numberOfViews) ),
							display: 'block',
						});
						$(items[i]).animate(
							{
								right: ( (i*width) ),
							}, 
							properties.speed
						);
					}

				}
			}
		});
				
	});
})(jQuery, window, document);