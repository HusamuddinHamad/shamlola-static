(function ($, d, w) {
	
	$(d).ready(function () {
		$('.questions article .image-holder').circle();

		$('form.ask .dropdown .dropdown-menu a').click(function (e) {
			e.preventDefault();
			$('form.ask .dropdown .title').text( $(this).text() );
		})
	});

})(jQuery, document, window);