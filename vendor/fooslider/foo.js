(function (w, $, d) {
	$.fn.extend({
		fooSlide: function (data) {
			var that = this;

			this._init = function (data) {
	            
	            if ( data == undefined )
	                this._data = null;
	            else 
	            	this._data = data;


				this._prepareProperties();

				this._prepareActions();
				this._events();
	            
	            if ( typeof(this._data.paginationContainerClass) === 'string' )
	            	this._pagination();

	            if ( typeof(this._data.pagination) === 'boolean' && this._data.pagination === true ) {

	            	this.find('[data-paged]').click(function (e) {
	            		e.preventDefault();
		            	if ( that._properties._paginationWait === true && that._properties.index !== $(this).data('paged') )
							that._action( $(this).data('paged') );
	            	});
					
	            	$(w).resize(function () {
	            		that.find('[data-paged]').removeClass('active').first().addClass('active');
	            	})

					this._properties._paginationWait = false;
					this._properties._paginationTimer = setTimeout(function () {
						that._properties._paginationWait = true;
					}, this._properties.speed);
	            }
			}

			this._pagination = function () {
				var pages = Math.ceil( this._properties.itemsLength / this._properties.numberOfViews );
				this._properties.paginationContainerDom.html('');
				for(var i=0; i < pages; i++) {
					var active = Math.ceil(this._properties.index / this._properties.numberOfViews) === i;
					
					this._properties.paginationContainerDom.append(
						'<a data-paged='+ (i*this._properties.numberOfViews) +' class="fa paged '+ ( active ? 'fa-circle active':'fa-circle-o' ) +'" href="#"></a>'
					);
				}

				this._properties.paginationContainerDom.find('.paged').click(function (e) {
					e.preventDefault();

					if ( that._properties._paginationWait === true && that._properties.index !== $(this).data('paged') )
						that._action( $(this).data('paged') );

					that._properties._paginationWait = false;
					that._properties._paginationTimer = setTimeout(function () {
						that._properties._paginationWait = true;
					}, that._properties.speed);
				});

			}

			this._prepareProperties = function () {

				this._properties = {
					index: 						0,
					numberOfViews: 			1,
					_numberOfViewsObject: 	null,
					render: 						null,
					itemsParentClass: 		'items',
					nextButtonClass: 			'previous-button',
					previousButtonClass: 	'next-button',
					itemsPositions: 			[],
					mobile: 						false,
					autoplay: 					false,
					itemsContainer:			null,
					items: 						null,
					itemsLength:				null,
					nextButton: 				null,
					previousButton:			null,
					speed:						1000,
					ready: 						true,
					_timer: 						null,
					_interval: 					null,
					_intervalAction: 			null,
					_intervalTimer: 			null,
					_paginationWait: 			true,
					_paginationTimer: 		null,
					_data: 						null,
				};

				if ( this._data != null ) {
					$.each(this._data, function (key, value) {
						that._properties[key] = value;
					});
				}

				this._properties.itemsContainer 		= $(this).find('.'+this._properties.itemsParentClass);
				this._properties.items 					= this._properties.itemsContainer.children();
				this._properties.nextButton			= $(this).find('.'+this._properties.nextButtonClass);
				this._properties.previousButton		= $(this).find('.'+this._properties.previousButtonClass);
				this._properties.itemsLength			= this._properties.items.length;

            if ( typeof(this._data.paginationContainerClass) === 'string' )
					this._properties.paginationContainerDom = this.find('.'+this._data.paginationContainerClass);

				if ( typeof(this._properties.numberOfViews) === 'object' ) {
					this._numberOfViewsObject = this._properties.numberOfViews;
					
					this._setNumberOfViews = function () {

						for( var key in that._numberOfViewsObject ) {

							if ( !!parseInt(key) ) {
								
								if ( $(w).width() < parseInt(key) )
									that._properties.numberOfViews = that._numberOfViewsObject[key];

							} else {

								switch( key ) {
									case 'xs':
										if ( $(w).width() < 768 )
											that._properties.numberOfViews = that._numberOfViewsObject[key];

										break;
									case 'sm':
										if ( $(w).width() >= 768 )
											that._properties.numberOfViews = that._numberOfViewsObject[key];

										break;
									case 'md':
										if ( $(w).width() > 992 )
											that._properties.numberOfViews = that._numberOfViewsObject[key];

										break;
									case 'lg':
										if ( $(w).width() > 1200 )
											that._properties.numberOfViews = that._numberOfViewsObject[key];

										break;

								}
							}

						}

					}

					this._setNumberOfViews();
					$(w).resize( this._setNumberOfViews );
				}
			}

			this._prepareActions = function () {

				// autoplay reader
				if ( this._properties.autoplay != false ) {
					if ( typeof(this._properties.autoplay) === 'boolean' )
						this._properties._interval = 5000;
					else 
						this._properties._interval = this._properties.autoplay;

					clearInterval(this._properties._intervalAction);
					this._properties._intervalAction = setInterval(function () {
						that._action(that._properties.index+that._properties.numberOfViews);
					}, this._properties._interval);
				}

				if (typeof(this._data.render) === 'function') {
					this.render = function () {
						this._data.render(that._properties, that);
					}
					this.render();
				}

			}

			this._events = function () {

				if ( typeof(this._properties.mobile) === 'boolean' && this._properties.mobile === true ) {
					this.on('swipeleft', function () {
						that._properties.previousButton.trigger('click');
					});

					this.on('swiperight', function () {
						that._properties.nextButton.trigger('click');
					});
				}

				this._properties.nextButton.bind('click', function (e) {
					e.preventDefault();
					if ( that._properties.ready === true )
						that._action(that._properties.index+that._properties.numberOfViews);
				});

				this._properties.previousButton.bind('click', function (e) {
					e.preventDefault();
					if ( that._properties.ready === true )
						that._action(that._properties.index-that._properties.numberOfViews);
				});
				
				if ( this._properties.autoplay != false ) {
					this.on('mousemove', function () {
						clearTimeout(that._properties._intervalTimer);
						clearInterval(that._properties._intervalAction);
						that._properties._intervalTimer = setTimeout(function () {
							that._properties._intervalAction = setInterval(function () {
								that._action(that._properties.index+that._properties.numberOfViews);
							}, that._properties._interval);
						}, that._properties._interval/0.6);
					});			
				}


				var tmp_timer;
				$(w).resize(function () {

					clearTimeout(tmp_timer);
					tmp_timer = setTimeout(function () {
						
						that._properties.itemsPositions = [];
						that._properties._data = null;
						that._properties.items.css('display', 'block');

						that._prepareActions();
						that._properties.index = 0;
						
						if ( that._properties.paginationContainerDom )
							that._pagination();

					}, 100);


				});
			}

			this._action = function (index) {
				if ( typeof(this._properties.action) === 'function' ) {
					
					var direction 	= this._properties.index - index;
						direction 	= direction / Math.abs( direction );

					index = that._validateIndex(index);
					this._properties.index = index;

					if ( this._properties._data == null )
						var oldData = this._properties.itemsPositions.slice(0);
					else 
						var oldData = this._properties._data.slice(0);

					this._properties._data = this._properties.itemsPositions.slice(0);
					for ( var i=1; i<=index; i++ ) {
						var lastItem = this._properties._data.pop();
						this._properties._data.unshift(lastItem);
					}
					
					var max = Math.max.apply(null, this._properties._data);
					var min = Math.min.apply(null, this._properties._data);
					var inView = this._properties._data.indexOf(0);

					this._properties.action(this._properties, this, index, direction, this._properties._data, inView, oldData, max, min);
					if ( this._properties.paginationContainerDom )
						this._pagination();
					
					// wait till finishes
					this._properties.ready = false;
					clearTimeout( this._properties._timer );
					this._properties._timer = setTimeout(function () {
						that._properties.ready = true;
					}, this._properties.speed);

				}


			}

			this._validateIndex = function (index) {

				if ( index <= -1 )
					index = this._properties.itemsLength - Math.abs(index);

				return Math.abs( index%this._properties.itemsLength );

			}

            this._init(data); // trigger init

		},
	});

})(window, jQuery, document);