module.exports = function(grunt) {

	// Project configuration.
	grunt.initConfig({

		pkg: grunt.file.readJSON('package.json'),
		
		watch: {
			options: {
				livereload: 9000
			},
			Gruntfile: {
				files: 'Gruntfile.js',
				tasks: ['haml', 'sass'],
			},
			css: {
				files: ['src/sass/*.scss', 'src/sass/partials/*.scss'],
				tasks: ['sass'],
			},
			haml: {
				files: ['src/haml/*.haml'],
				tasks: ['haml'],
			},
			javascript: {
				files: ['build/js/*.js', 'vendor/fooslider/*.js']
			},
			svg: {
				files: ['draw.html']
			}
		},

		sass: {
			dist: {
				options: {
					style: 'expanded',
					noCache: true,
				},
				files: {
					'build/css/main.css': 'src/sass/main.scss'
				}
			}
		},
		
		haml: {
			dist: {
				files: {
					/*'index.html': [
						'src/haml/header.haml',
						'src/haml/main-slider.haml',
						'src/haml/index.haml',
						'src/haml/footer.haml',
					],
					'cat.html': [
						'src/haml/header.haml',
						'src/haml/cat.haml',
						'src/haml/footer.haml',
					],
					'cat1.html': [
						'src/haml/header.haml',
						'src/haml/cat1.haml',
						'src/haml/footer.haml',
					],
					'cat2.html': [
						'src/haml/header.haml',
						'src/haml/cat2.haml',
						'src/haml/footer.haml',
					],
					'recipe.html': [
						'src/haml/header.haml',
						'src/haml/recipe.haml',
						'src/haml/footer.haml',
					],
					'recipes.html': [
						'src/haml/header.haml',
						'src/haml/recipes.haml',
						'src/haml/footer.haml',
					],
					'book.html': [
						'src/haml/header.haml',
						'src/haml/book.haml',
						'src/haml/footer.haml',
					],
					'books.html': [
						'src/haml/header.haml',
						'src/haml/books.haml',
						'src/haml/footer.haml',
					],
					'chef.html': [
						'src/haml/header.haml',
						'src/haml/chef.haml',
						'src/haml/footer.haml',
					],
					'ask.html': [
						'src/haml/header.haml',
						'src/haml/ask.haml',
						'src/haml/footer.haml',
					],
					'questions.html': [
						'src/haml/header.haml',
						'src/haml/questions.haml',
						'src/haml/footer.haml',
					],
					'community.html': [
						'src/haml/header.haml',
						'src/haml/community.haml',
						'src/haml/footer.haml',
					],
					'single-community.html': [
						'src/haml/header.haml',
						'src/haml/single-community.haml',
						'src/haml/footer.haml',
					],*/
					
					'test.html': [
						'src/haml/header.haml',
						'src/haml/test.haml',
						'src/haml/footer.haml',
					],

				}
			}
		},

	});

	// Load the plugins
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-haml');

	// grunt.loadNpmTasks('grunt-contrib-uglify');
	// grunt.loadNpmTasks('grunt-contrib-concat');

	// Default task(s).
	grunt.registerTask('default', ['watch']);

};